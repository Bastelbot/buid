<?php
namespace BUID\Service;

class BUID {
    const clearVer = 15;  // 00001111  Clears all bits of version byte with AND
    const clearVar = 63;  // 00111111  Clears all relevant bits of variant byte with AND
    const varRFC   = 128; // 10000000  The RFC 4122 variant (this variant)
    const version4 = 64;  // 01000000
    const versionB = 176; // 10110000
    const charset  = '0123456789ABCDEFGHJKMNPQRSTVWXYZ';

/*
    uuid-4; // random UUID
    uuid-8; // random UUID without variant information
    hex-06; // 6 random bytes hex encoded // 12 alphanumeric case-insensitive characters
            // good for passwords or short-time tokens
    b32-10; // 10 random bytes base32 encoded // 16 alphanumeric case-insensitive characters
            // good for long-time tokens or IDs with central master
    b32-20; // 20 random bytes base32 encoded // 32 alphanumeric case-insensitiv characters
            // good for IDs without central master
    b64-09;
    b64-12;
    b64-15;
    b64-24;
*/

    public static function generate($type)
    {
        list($enc, $len) = explode('-', $type);
        switch($enc) {
            case 'uuid':
                return self::genUUID($len);   // uuid-4 / uuid-8
            case 'raw':
            case 'hex':
            case 'b32':
            case 'b64':
                return self::genBUID($enc, $len);
            default:
                return false;
        }
    }

    public static function genUUID($type)
    {
        switch($type) {
            case 4:
                $rnd = self::randomBytes(16);
                $rnd[8] = chr(ord($rnd[8]) & self::clearVar | self::varRFC);
                $rnd[6] = chr(ord($rnd[6]) & self::clearVer | self::version4);
                $enc = bin2hex($rnd);
                $enc =  substr($enc,0,8)."-".
                        substr($enc,8,4)."-".
                        substr($enc,12,4)."-".
                        substr($enc,16,4)."-".
                        substr($enc,20,12);
                return $enc;
            case 8:
                $uid = new bUID;
                $rnd = self::randomBytes(16);
                $rnd[6] = chr(ord($rnd[6]) & self::clearVer | self::version4);
                $enc = bin2hex($rnd);
                $enc =  substr($enc,0,8)."-".
                        substr($enc,8,4)."-".
                        substr($enc,12,4)."-".
                        substr($enc,16,4)."-".
                        substr($enc,20,12);
                return $enc;
            default:
                return false;
        }
    }

    public static function genBUID($encoding, $entropy)
    {
        $rnd = self::randomBytes($entropy);
        $enc = self::encode($rnd, $encoding);
        $split = self::enc2split($encoding);
        if($split) $enc = implode('-', str_split($enc, $split));
        return $enc;
    }

    protected static function enc2split($encoding)
    {
        switch($encoding) {
            case 'raw': return 0;   // don't split raw
            case 'hex': return 6;
            case 'b32': return 4;
            case 'b64': return 8;
            default: return false;
        }
    }

    public static function encode($data, $encoding)
    {
        switch($encoding) {
            case 'raw': return $data;
            case 'hex': return bin2hex($data);
            case 'b32': return self::base32_encode($data);
            case 'b64': return self::base64_encode($data);
            default: return false;
        }
    }

    public static function base32_encode ($data) {
        /**

            encode $data into a human friendly character set
            the encoded data is case insensitiv

        **/
        $charset = self::charset;                                   // charset must have 32 characters!
        $chrs = unpack('C*', $data);                                // extract single ascii codes
        $bin = vsprintf(str_repeat('%08b', count($chrs)), $chrs);   // convert to binary

        $length = strlen($bin);                                     // pad required bits ...
        $padlen = ($rest=$length%40) ? 40-$rest : 0;                // if length($bin) ...
        if ($padlen) $bin .= str_repeat('0', $padlen);              // is not a multiple of 40

        $bin = preg_replace('/(.{5})/', '000$1', $bin);             // pad each 5bit-group with 3 '0'
        preg_match_all('/.{8}/', $bin, $chrs);                      // select 8bit-groups
        $chrs = array_map(function ($str) use ($charset) {          // replace 8bit-groups with char
                return $charset[bindec($str)];
            }, $chrs[0]);

        $enc = strtolower(join('', $chrs));                         // to string // code padded as '='
        if ($padlen) $enc = str_pad(substr($enc, 0, -floor($padlen/5)), strlen($enc), '=');
        return $enc;
    }

    public static function base64_encode ($data) {
        /**

            encode $data into a printable character set
            the encoded data is case sensitive

        **/
        $data = base64_encode($data);
        $data = str_replace('/', '_', $data);
        $data = str_replace('+', '-', $data);
        return $data;
    }

    public static function randomBytes($bytes) {
        /**
            get $bytes random bytes from either
            system random generator or
            mt_rand, the better php random generator
        **/
        if (is_readable('/dev/urandom')) {
            // try to read from system random generator
            $source = fopen('/dev/urandom', 'rb');
            $rand = fread($source, $bytes);
            fclose ($source);
            return $rand;
        }
        $rand = '';
        for ($a = 0; $a < $bytes; $a++) {
            $rand .= chr(mt_rand(0, 255));
        }
        return $rand;
    }

}
